module.exports = function (BaseReader) {
  class NationalPublicRadioReader extends BaseReader {
    static get basename () {
      return 'www.npr.org'
    }

    static get source () {
      return 'National Public Radio'
    }

    get author () {
      return this.$('p a[rel=author]').text().trim()
    }

    get content () {
      let html = ''
      let that = this
      this.$('#storytext > p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      return this.$('.dateblock time').attr('datetime')
    }

    get title () {
      return this.$('h1').text()
    }
  }
  return NationalPublicRadioReader
}
