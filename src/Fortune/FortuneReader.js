/**
 * Implementation of the BaseReader to handle Fortune Magazine articles.
 */
module.exports = function (BaseReader) {
  class FortuneReader extends BaseReader {
    static get basename () {
      return 'fortune.com'
    }

    static get source () {
      return 'Fortune'
    }

    get author () {
      return this.$('.keyvals').attr('data-content_author_name')
    }

    get content () {
      let html = ''
      let that = this
      this.$('#article-body .padded p').each(function () {
        html += that.$('<div>').append(that.$(this).clone()).html()
      })
      return html
    }

    get datetime () {
      // let json = this.$('script[type="application/ld+json"]').text();
      // let array = JSON.parse(json);
      // for (let schema of array) {
      //     if (schema.@type === 'NewsArticle') {
      //         return schema.dateModified;
      //     }
      // }
      // return this.$('meta[name=date]').attr('content');
      return this.$('.keyvals').attr('data-content_published_date')
    }

    get title () {
      return this.$('h1').text()
    }
  }
  return FortuneReader
}
